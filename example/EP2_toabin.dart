import 'dart:io';

void main() {
  printMenu(menu());
  int numMenu = int.parse(stdin.readLineSync()!);
  var customerMenu = selectMenu(menu(), numMenu);

  print("\nThe menu of you choose is: $customerMenu   $Topping");
  printPay();

  int payment = int.parse(stdin.readLineSync()!);
  printPrice(payment);

  print('-------------- Please wait the order --------------');
}

String Topping = ' ';
int? indexTopping;
String selectMenu(List<String> menu, int numMenu) {
  var selectOfMenu = menu[numMenu - 1];
  return topping(selectOfMenu, menuTopping());
}

String topping(String menuCustommer, List<String> menuTopping) {
  if (menuCustommer.contains("ICE")) {
    print('------------Please select the TOPPIND---------------');
    for (int i = 0; i < menuTopping.length; i++) {
      stdout.write('${menuTopping[i]}   INPUT   ${i + 1}  \n');
    }
    int selectTopping = int.parse(stdin.readLineSync()!);
    Topping = menuTopping[selectTopping - 1];
    indexTopping = selectTopping - 1;
    print('---------------------------------------------------');
  }
  return menuCustommer;
}

void printMenu(List menu) {
  print('\n');
  print('------------- Please select the menu -------------');
  for (int i = 0; i < menu.length; i++) {
    stdout.write('${menu[i]}   INPUT   ${i + 1}  \n');
  }
  print('---------------------------------------------------');
}

void printPay() {
  print('------------- Please select a payment -------------');
  var payment = ['cash', 'QR code', 'credit card', 'rabbit pay'];
  for (int i = 0; i < payment.length; i++) {
    stdout.write('${payment[i]}   INPUT   ${i + 1} \n');
  }
  print('-----------------------------------------------------');
}

/*void printPayAgain(int choidSuc) {
  print('------------- Please select a payment -------------');
  var payment = ['cash', 'QR code', 'credit card', 'rabbit pay'];
  int x = choidSuc;
  while (choidSuc == 2) {
    for (int i = 0; i < payment.length; i++) {
      stdout.write('${payment[i]}   INPUT   ${i + 1} \n');
    }
    print('-----------------------------------------------------');
    print('If payment is successful, please input 1');
    print('\nIf your change payment method 2');
    x = int.parse(stdin.readLineSync()!);
  }
  print('-----------------------------------------------------');
  print('\nIf payment is successful, please input 1');
  print('\nIf your change payment method 2');
  int paymentSuc = int.parse(stdin.readLineSync()!);
  if (paymentSuc == 2) {
    int paymentAain = int.parse(stdin.readLineSync()!);
    return printPay(paymentAain);
  }
}*/

void printPrice(int input) {
  var price = [30, 35, 30, 40, 45, 40, 50, 55, 40];
  var priceTopping = [20, 15, 10];
  if (Topping != ' ') {
    print(
        '\nPrice of Menu: ${price[input - 1]} Baht. \nPrice of Topping: ${priceTopping[indexTopping!]} Baht. \nPrice Total: ${price[input - 1] + priceTopping[indexTopping!]} Baht.');
  } else {
    print(
        '\nPrice of Menu: ${price[input - 1]} Baht. \nPrice Total: ${price[input - 1]} Baht.');
  }
}

List<String> menu() {
  var menu = [
    'HOT CAFE LATTE',
    'HOT CAPUCCINO',
    'HOT MATCHA LATTE',
    'ICED CALAMEL CAFELATTE',
    'ICED TAWANESE TEA CAFFE LATTE',
    'ICED ESPRESSO',
    'ICED THAI MILK TEA',
    'ICED TEA',
    'ICED LIMEADE TEA'
  ];
  return menu;
}

List<String> menuTopping() {
  var menu = ['BROWN SUGAR', 'BROWA HONEY', 'FRUIT SALAD'];
  return menu;
}
